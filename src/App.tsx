import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Link, Routes, Route } from 'react-router-dom';
import { Container, Nav, Navbar } from 'react-bootstrap';
import Login from './components/pages/Login/Login';
import Home from './components/pages/Home/Home';
import Register from './components/pages/Register/Register';
import Notfound404 from './components/pages/Error/404/404';
import Homework from './components/pages/Homework7/homework';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar bg="light" expand="lg">
          <Container>
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <Nav.Link as={Link} className="App-link" to="/">Home Page</Nav.Link>
                <Nav.Link as={Link} className="App-link" to="/login">Login Page</Nav.Link>
                <Nav.Link as={Link} className="App-link" to="/register">Register Page</Nav.Link>
                <Nav.Link as={Link} className="App-link" to="/404">Not found</Nav.Link>
                <Nav.Link as={Link} className="App-link" to="/homework">Homework7</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/login' element={<Login />} />
          <Route path='/register' element={<Register />} />
          <Route path='/homework' element={<Homework />} />
          <Route path='/404' element={<Notfound404 />} />
          <Route path="*" element={<span>404</span>} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
