import { ComponentClass } from "../classComponent";
import { ComponentFunc } from "../funcComponent";

const Homework = () => {
    return (
        <div>Homework 7
            <div className="Component-container">
                <ComponentClass></ComponentClass></div>
            <div className="Component-container">
                <ComponentFunc></ComponentFunc>
            </div>
        </div>
    );
}
export default Homework;