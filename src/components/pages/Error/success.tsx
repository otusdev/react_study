import { Component } from "react";
import '../../../App.css';

interface Props {
    message: string;
}
export function Success(props: Props) {
    return <div className="App-success">
        {props.message}
    </div>;
}
