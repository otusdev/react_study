import { Component } from "react";
import '../../../App.css';

interface Props {
    message: string;
}
export function Error(props: Props) {
    return <div className="App-error">
        {props.message}
    </div>;
}
