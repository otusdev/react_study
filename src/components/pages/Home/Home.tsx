import ComponentGetStore from "../../componentGetStore";
import { ComponentSetStore } from "../../componentSetStore";
// import styles from '../../../App.css';
const Home = () => {
    return (
        <div className="Page-container"> <span>Home Page</span>
            <ComponentSetStore />
            <ComponentGetStore />
        </div>
    );
}
export default Home;