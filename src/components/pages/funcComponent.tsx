import { useState } from 'react';
import { Error } from './Error/error';
import axios from "axios";
import { Success } from './Error/success';

export function ComponentFunc() {

  const [url, setUrl] = useState<string>('https://catfact.ninja/fact');
  const [errorMessage, setError] = useState<string>('');
  const [result, setResult] = useState<string>('');

  const urlPatternValidation = (URL: any) => {
    const regex = new RegExp('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?');
    return regex.test(URL);
  };

  const click = () => {
    setError("");
    setResult("");
    if (urlPatternValidation(url))
      axios(url)
        .then(response => {
          setResult(response.data?.fact)
          alert("Success")
        })
        .catch(error => {
          setError(error.message)
        });
    else {
      setError("Url is not valid")
    }
  }

  return <div>
    <input value={url} onChange={e => setUrl(e.target.value)} />
    <button onClick={click}>
      Отправить
    </button>
    <div className="App-request-result"></div>
    <Success message={result}></Success>
    <Error message={errorMessage}></Error>
  </div>;
}
