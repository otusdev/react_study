import axios from "axios";
import { Component, } from "react";
import { Error } from './Error/error';

interface State {
    url: string;
    requestResult: any;
    error: string;
}

export class ComponentClass extends Component<{}, State> {

    constructor(props: any) {
        super(props);
        this.state = {
            url: 'https://catfact.ninja/fact',
            requestResult: '',
            error: ''

        };
    }
    urlPatternValidation = (URL: any) => {
        const regex = new RegExp('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?');
        return regex.test(URL);
    };

    click = () => {
        this.setState({ error: '' , requestResult:''});
        if (this.urlPatternValidation(this.state.url))
            axios(this.state.url)
                .then(response => {
                    this.setState({ requestResult: response.data?.fact })
                    alert("Success")
                })
                .catch(error => {
                    this.setState({ error: error.message })
                });
        else {
            this.setState({ error: "Url is not valid" })
        }
    }

    render() {
        return <div>
            <input value={this.state.url} onChange={e => this.setState({ url: e.target.value })} />
            <button onClick={this.click}>
                Отправить
            </button>
            <div className="App-request-result">{this.state.requestResult}</div>
            <Error message={this.state.error}></Error>
        </div>;
    }
}
