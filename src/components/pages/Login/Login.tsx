import React, { useState } from 'react';
import styles from './Login.module.css';

const Login: React.FC = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    // Отправляем запрос к API с использованием fetch
    try {
      const response = await fetch('/developers/postbin/1705831431644-9108441413845?hello=world', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username, password }),
      });

      // Обрабатываем ответ от API
      const data = await response.json();
      console.log('Ответ от API:', data);
    } catch (error) {
      console.error('Ошибка при отправке запроса к API:', error);
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles.wrapLogin}>
        <form onSubmit={handleSubmit} className={styles.form}>
          {/* <div className={styles.title}>Account Login</div> */}

          <span className={styles.title}>Sign In	</span>
          <div className={styles.inputBlock}>
            <label htmlFor="username" className={styles.label}>
              Username:
            </label>
            <input
              type="text"
              id="username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              className={styles.input}
            />
          </div>
          <div>
            <label htmlFor="password" className={styles.label}>
              Password:
            </label>
            <input
              type="password"
              id="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              className={styles.input}
            />
          </div>
          <div className={styles.forgotBlock}>
            <span className={styles.forgot}>
              Forgot
            </span>
            <a href="#" className={styles.hyperlink} >
              Username / Password?
            </a>
          </div>
          <button type="submit" className={styles.button}>
            Log In
          </button>
        </form>
      </div>
    </div>
  );
};

export default Login;
