import { TextField } from "@mui/material";
import { connect } from "react-redux";

interface Props {
    data?: any;
}
const ComponentOutput = (props: Props) => {
    return <div className="Component-container">
        <div><b>Get data from store</b></div>
        <div className="item">
            <TextField disabled label="Get store" variant="outlined" value={props.data}></TextField>
        </div>
    </div>
}
const mapStateToProps = (state: any) => {  
    return { data: state.text.text };
}

export default connect(mapStateToProps)(ComponentOutput);
