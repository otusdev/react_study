import { Button, TextField } from "@mui/material";
import { useState } from "react";
import { connect, useDispatch } from "react-redux";
import { Actions } from "../stateManagement/reducer";

export function ComponentSetStore(){

    const dispatch = useDispatch();
    const [text, setText ] = useState<string>('');

    const click = () => {
        console.log(text)
        if (text) dispatch(Actions.setData(text))
    }
      
    return <div className="Component-container">
            <div className="item">
                <b>Set data to store</b>
            </div>
            <div className="item">
                <TextField label="Enter text" variant="outlined" onChange={e => setText(e.target.value)}></TextField>
            </div>
            <div className="item">
                <Button variant="contained" onClick={click}>Send to store.</Button>
            </div>      
    </div>
}
